#!/usr/bin/env python3
#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Script to send G-code file to Grbl controller.

Features

1. Displays Grbl controller status, current position, G-code line number
   and G-code line.
2. Supports hold and cycle commands with `!` and `~` keys.
3. Allows to continue processing after M0 G-code instruction.
"""

import argparse
import asyncio
import collections
import functools
import logging
import re
import sys

import readchar
import serial

logging.basicConfig()
logger = logging.getLogger(__name__)

# function to format controller status
fmt_status = '\r{status}: {x}, {y}, {z}  | {k}: {gcode:<40.40}'.format

# Grbl controller status parser
RE_STATUS = re.compile(r"""
    ([A-Z][a-z]+)\| (?# status like Hold, Idle, Run, etc.)
    MPos:(-?\d+\.\d{3}),(-?\d+\.\d{3}),(-?\d+\.\d{3}) (?# current pos)
    """, re.VERBOSE
)

class ControllerError(Exception): pass
class StatusError(ControllerError): pass
class GCodeError(ControllerError): pass
class ExitError(ControllerError): pass


def read_gcode_file(f):
    """
    Read G-code file, strip the whitespace and enumerate lines.
    
    :param f: File object.
    """
    lines = enumerate(f, 1)
    lines = ((i, line.strip()) for i, line in lines)
    return ((i, line + '\n') for i, line in lines if line)


def readline(dev):
    """
    Read line from serial device.

    The line is converted to string and stripped.

    :param dev: Serial device object.
    """
    return dev.readline().decode().strip()


def send_gcode(dev, k, gcode):
    """
    Send G-code line to serial device.

    :param dev: Serial device object.
    :param k: Line number of G-code line.
    :param gcode: G-code line.
    """
    if __debug__:
        logger.debug('send line {}: {}'.format(k, gcode))

    dev.write(gcode.encode())

    # expect 'ok' or empty string, the latter can happen with M0
    response = readline(dev)

    if 'error' in response:
        raise GCodeError('error:{}:{}'.format(k, gcode.strip()))


def controller_status(dev, k, gcode):
    """
    Read controller status and display general status on the screen.

    The general status consists of controller status, current position,
    G-code line number and G-code line.

    :param dev: Serial device object.
    :param k: Line number of G-code line.
    :param gcode: G-code line.
    """
    dev.write(b'?')
    line = readline(dev)
    # we can still get `ok` or empty string here, i.e. due to M0
    if line == 'ok' or line == '':
        return ''

    match = RE_STATUS.search(line)
    if not match:
        raise StatusError('Cannot process device status: {}'.format(line))

    status, x, y, z = match.groups()

    t = fmt_status(status=status, x=x, y=y, z=z, k=k, gcode=gcode.strip())
    print(t, end='')
    return status


@asyncio.coroutine
def keyboard(q_cmd):
    """
    Read key and generate application command.

    Recognized keys are `!` (hold) and `~` (cycle).

    This is coroutine.

    :param q_cmd: Application command queue.
    """
    loop = asyncio.get_event_loop()
    commands = {
        b'!': 'hold',
        b'~': 'cycle',
    }
    while True:
        key = yield from loop.run_in_executor(None, readchar.readchar)
        cmd = commands.get(key)
        if cmd:
            yield from q_cmd.put(('control', cmd))


@asyncio.coroutine
def file_sender(fn, q_cmd, buffer):
    """
    Open G-code file and send it to G-code line buffer.

    Once the whole file is sent to the buffer, the `eof` application
    command is generated.

    This is coroutine.

    :param fn: G-code filename.
    :param q_cmd: Application command queue.
    :param buffer: G-code line buffer.
    """
    with open(fn) as f:
        for k, gcode in read_gcode_file(f):
            yield from buffer.put((k, gcode))
    yield from q_cmd.put(('eof', None))


@asyncio.coroutine
def grbl_controller(dev, q_cmd, buffer):
    """
    Grbl controller main loop.

    Receives application commands and G-code lines to be sent to Grbl
    controller.

    :param dev: Serial device object.
    :param q_cmd: Application command queue.
    :param buffer: G-code line buffer.
    """
    k = 0
    gcode = ''

    dev.write(b'\n')
    s = readline(dev)
    dev.write(b'\x18')
    s = readline(dev)
    s = readline(dev)
    if not s.startswith('Grbl'):
        raise StatusError('Cannot reset device')

    if __debug__:
        logger.debug('device initialized')
    
    status = controller_status(dev, k, gcode)
    eof = False

    while True:
        cmd, param = (None, None) if q_cmd.empty() else q_cmd.get_nowait()
        if __debug__:
            logger.debug('got command: {}'.format(cmd))

        status = controller_status(dev, k, gcode)

        if cmd == 'control':
            if param == 'hold':
                dev.write(b'!')
            elif param == 'cycle':
                dev.write(b'~')

        elif cmd == 'eof':
            eof = True

        # all g-code lines are sent and last line is processed
        if eof and not buffer.qsize() and status == 'Idle':
            raise ExitError()

        if status == 'Idle' and buffer.qsize():
            k, gcode = buffer.get_nowait()
            send_gcode(dev, k, gcode)

        yield from asyncio.sleep(0.1)


parser = argparse.ArgumentParser(description='G-code sender')
parser.add_argument(
    '-v', '--verbose', dest='verbose', action='store_true',
    help='enable debug log'
)
parser.add_argument('input', help='G-code filename')
parser.add_argument('device', help='Device file')
args = parser.parse_args()

if args.verbose:
    logger.setLevel(logging.DEBUG)

# set timeout to 1s, some commands might not return status, i.e. M0;
# because of timeout we get empty string, which we will consider valid
# response
dev = serial.Serial(args.device, 115200, timeout=1)
loop = asyncio.get_event_loop()

# application command queue
q_cmd = asyncio.Queue(maxsize=5)

# g-code line buffer
buffer = asyncio.Queue(maxsize=100)

t1 = grbl_controller(dev, q_cmd, buffer)
t2 = file_sender(args.input, q_cmd, buffer)
t3 = keyboard(q_cmd)
task = asyncio.gather(t1, t2, t3)

try:
    loop.run_until_complete(task)
except (GCodeError, StatusError) as ex:
    task.cancel()
    print()
    print(str(ex), file=sys.stderr)
    sys.exit(2)
except ExitError as ex:
    print()
finally:
    dev.close()

if __debug__:
    logger.debug('done')

# vim: sw=4:et:ai
