# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
CAD shapes, which can be processed with G-code renderer.
"""

class Shape(object):
    def __init__(self, width, height, depth):
        super().__init__()
        self.width = width
        self.height = height
        self.depth = depth
        self.pad = (0, 0)

class Rect(Shape):
    def __init__(self, width, height, depth, border=0, radius=0):
        super().__init__(width, height, depth)
        self.border = border
        self.radius = radius

class Pocket(Shape):
    def __init__(self, width, height, depth, radius=0):
        super().__init__(width, height, depth)
        self.radius = radius

class Bore(object):
    def __init__(self, diameter, depth):
        super().__init__()
        self.diameter = diameter
        self.depth = depth

    @property
    def width(self):
        return self.diameter

    @property
    def height(self):
        return self.diameter

class FrameOf(Shape):
    def __init__(self, shape, cols, rows, width, height):
        super().__init__(width, height, 0)
        self.cols = cols
        self.rows = rows
        self.shape = shape

    @property
    def border(self):
        if self.shape.width != self.shape.height:
            raise NotImplementedError(
                'Two dimensional frame borders not supported yet'
            )
        return self.shape.width

__all__ = ['Bore', 'FrameOf', 'Pocket', 'Rect', 'Shape']

# vim: sw=4:et:ai
