# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tool support.
"""

import threading

class ToolContext(object):
    def __init__(self, tool, feed_rate):
        self.tool = tool
        self.feed_rate = feed_rate
        self.plunge_rate = feed_rate / 2
        self.cut_depth = 0.25 * tool.diameter


    def __enter__(self):
        set_context(self)
        return self


    def __exit__(self, *args):
        set_context(None)



class Tool(object):
    def __init__(self, diameter, distance):
        """
        Tool instance.

        :var diameter: Tool diameter.
        :var distance: Tool distance to material.
        :var radius: Tool radius.
        """
        super().__init__()
        self.diameter = diameter
        self.radius = diameter / 2
        self.distance = distance


_local = threading.local()

def set_context(ctx):
    _local.__dcad_context = ctx


def get_context():
    return _local.__dcad_context


# vim: sw=4:et:ai
