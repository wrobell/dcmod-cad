# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
G-code renderer.
"""

import logging
from functools import singledispatch, partial
from itertools import accumulate as cumsum, islice, repeat, chain

from .shape import *
from . import gcode as gc
from . tool import get_context


logger = logging.getLogger(__name__)


@singledispatch
def gcode(obj):
    raise NotImplementedError('Uknown object {}'.format(obj))


@gcode.register(str)
def _(s):
    return s


@gcode.register(tuple)
@gcode.register(list)
def _(items):
    f = lambda v: v if isinstance(v, str) else gcode(v)
    return '\n'.join([f(i) for i in items if i])


@gcode.register(Rect)
def _(shape):
    tool_ctx = get_context()
    tool = tool_ctx.tool

    cr = shape.radius - tool.radius if shape.radius > tool.radius else 0
    rect = partial(_rect_c, speed=tool_ctx.feed_rate, radius=cr)

    width = shape.width - tool.diameter
    height = shape.height - tool.diameter
    border = shape.border - tool.diameter

    start_w = width - border * 2
    start_h = height - border * 2

    step = tool.radius / 2 # FIXME: make it configurable
    steps = distance_step(border, step)
    sk = [s * 2 for s in steps]
    ws = distance_inc(start_w, sk)
    hs = distance_inc(start_h, sk)

    # move to starting point
    path = [
        gc.rapid_x(-start_w / 2 + cr),
        gc.rapid_y(start_h / 2),
    ]

    # initial cut
    with DepthRepeater(tool_ctx, shape.depth) as dr:
        dr.path.extend(rect(start_w, start_h))
    path.extend(dr.path)

    # complete border
    for w, h, s in zip(ws, hs, steps):
        path.extend([
            gc.x(-s, tool_ctx.feed_rate),
            gc.y(s, tool_ctx.feed_rate),
            rect(w, h),
        ])

    # go back to origin point
    path.extend([
        gc.rapid_z(tool.distance + shape.depth),
        gc.rapid_x(start_w / 2 + border - cr),
        gc.rapid_y(-start_h / 2 - border),
    ])

    return gcode(path)


@gcode.register(Pocket)
def _(shape):
    tool_ctx = get_context()
    tool = tool_ctx.tool
    feed_rate = tool_ctx.feed_rate

    width = shape.width - tool.diameter
    height = shape.height - tool.diameter
    cut = 0.75 * tool.diameter # FIXME: make it configurable

    if width < height:
        raise NotImplementedError('Pocket having height > width not supported yet')

    rect = partial(_rect_c, speed=feed_rate)
    spiral = partial(_spiral_c, speed=feed_rate)

    w_step = min(width, width - height + 2 * cut)
    cut = min(cut, height / 2)
    z_step = lambda d, plunge_rate: _slot(w_step, d, feed_rate, plunge_rate)

    path = [gc.rapid_x(-w_step / 2)]
    with DepthRepeater(tool_ctx, shape.depth, z_step=z_step) as dr:
        f_steps = lambda start, step, n: cumsum(chain([start], repeat(step, n)))

        # number of spirals to perform
        n = int((height / 2) // cut)

        # n - 1 spirals
        w_steps = f_steps(w_step, 2 * cut, n)
        h_steps = f_steps(cut, 2 * cut, n)
        steps = zip(w_steps, h_steps)
        if n > 0:
            dr.path.extend(spiral(w, h, cut) for w, h in islice(steps, n - 1))

        # last spiral
        w, h = next(steps)
        dr.path.append(spiral(w, h, cut, end=True))

        d = (width - w) / 2
        if d:
            dr.path.extend([
                gc.x(-d, feed_rate),
                spiral(w + d * 2, h + cut + d, d, end=True),
            ])
        dr.path.append([
            gc.y(height / 2, feed_rate),
            gc.x((width - w_step) / 2, feed_rate),
        ])

    path.extend(dr.path)

    # move to starting point
    path.extend([
        gc.rapid_z(shape.depth + tool.distance),
        gc.rapid_x(w_step / 2),
    ])
    return gcode(path)

@gcode.register(Bore)
def _(shape):
    tool_ctx = get_context()
    tool = tool_ctx.tool
    feed_rate = tool_ctx.feed_rate

    if tool.diameter > shape.diameter:
        raise DCadError(
            'Tool diameter larger than shape diameter {} {}'
            .format(tool.diameter, shape.diameter)
        )

    r = (shape.diameter - tool.diameter) / 2
    d = shape.depth + tool.distance
    if abs(r) < 1e-6:
        # TODO: grbl does not support G87
        t = [
            gc.rapid_z(-tool.distance),
            gc.z(-shape.depth, tool_ctx.plunge_rate),
            gc.rapid_z(d)
        ]
    else:
        # TODO: use different shape?
        assert r > 0, 'shape: {}, tool: {}'.format(
            shape.diameter, tool.diameter
        )

        dx = -r
        t = [gc.rapid_x(dx)]
        with DepthRepeater(tool_ctx, shape.depth) as dr:
            dr.path.append(gc.rc(0, 0, -dx, 0, feed_rate))
        t.extend(dr.path)
        t.extend([
            gc.rapid_z(shape.depth + tool.distance),
            gc.rapid_x(-dx),
        ])
    return gcode(t)


@gcode.register(FrameOf)
def _(shape):
    tool_ctx = get_context()
    tool = tool_ctx.tool
    w = shape.width - shape.shape.width
    h = shape.height - shape.shape.height

    n = shape.cols - 1
    m = shape.rows - 1
    dw = w / n
    dh = h / m

    def _spread(op_move, dist, n, end=True):
        path = [gcode(shape.shape), op_move(dist)] * (n - 1)
        path.append(gcode(shape.shape))
        if end:
            path.append(op_move(dist))
        return path

    return gcode([
        gc.rapid_x(-w / 2),
        gc.rapid_y(h / 2),
        _spread(gc.rapid_x, dw, n),
        _spread(gc.rapid_y, -dh, m),
        _spread(gc.rapid_x, -dw, n),
        _spread(gc.rapid_y, dh, m, end=False),
        # if odd number of rows, we are at the center of the height
        gc.rapid_y(h / 2) if shape.rows % 2 == 0 else None,
        gc.rapid_x(w / 2),
    ])


def distance_step(dist, step):
    """
    Return sequence of steps to cover given distance.

    :param dist: Distance to cover.
    :step: Length of single step.
    """
    n = int(dist / step)
    r = round(dist - n * step, 6)
    logger.debug('steps: n={}, step={}, r={}'.format(n, step, r))

    seq = [step] * n
    if r:
        seq.append(r)
    return seq

def distance_dec(dist, steps):
    """
    Return sequence of distances (except the initial distance), where each
    distance is decreased by a step.

    :param dist: Initial distance.
    :param steps: List of steps to apply to distance.
    """
    return [dist - s for s in cumsum(steps)]

def distance_inc(dist, steps):
    """
    Return sequence of distances (except the last distance), where each
    distance is increased by a step.

    :param dist: Initial distance.
    :param steps: List of steps to apply to distance.
    """
    return [dist + s for s in cumsum(steps)]

def _rect_cc(width, height, speed=None, radius=0):
    cr = radius
    w = width - cr * 2
    h = height - cr * 2
    return [
        gc.x(w, speed),
        gc.rcc(cr, cr, 0, cr) if cr else None,
        gc.y(-h, speed),
        gc.rcc(-cr, cr, -cr, 0) if cr else None,
        gc.x(-w, speed),
        gc.rcc(-cr, -cr, 0, -cr) if cr else None,
        gc.y(h, speed),
        gc.rcc(cr, -cr, cr, 0) if cr else None,
    ]


def _rect_c(width, height, speed=None, radius=0):
    cr = radius
    w = width - cr * 2
    h = height - cr * 2
    return [
        gc.x(w, speed),
        gc.rc(cr, -cr, 0, -cr, speed) if cr else None,
        gc.y(-h, speed),
        gc.rc(-cr, -cr, -cr, 0, speed) if cr else None,
        gc.x(-w, speed),
        gc.rc(-cr, cr, 0, cr, speed) if cr else None,
        gc.y(h, speed),
        gc.rc(cr, cr, cr, 0, speed) if cr else None,
    ]


def _spiral_cc(width, height, delta, speed, end=False):
    return [
        gc.x(width, speed),
        gc.y(height, speed),
        gc.x(-width - delta, speed),
        gc.y(-height - (0 if end else delta), speed),
    ]


def _spiral_c(width, height, delta, speed=None, end=False):
    return [
        gc.y(height, speed),
        gc.x(width, speed),
        gc.y(-height - delta, speed),
        gc.x(-width - (0 if end else delta), speed),
    ]


def _slot(width, depth, speed, plunge_rate):
    return [
        gc.z(depth / 2, plunge_rate), gc.x(width, speed),
        gc.z(depth / 2, plunge_rate), gc.x(-width, speed),
    ]


class DepthRepeater(object):
    """
    Repeat path multiple times until given depth is reached.

    Each intermediate depth step is determined by current tool diameter.

    This is context manager returning G-code path to be extended. The
    context manager client responsibility is to extend `DepthRepeater.path`
    list with G-code instructions. After leaving scope of context manager,
    the `path` variable contains updated path for each intermediate depth.

    :var tool_ctx: Current tool context.
    :var depth: Depth to be processed.
    :var x: X movement before going deeper.
    :var y: Y movement before going deeper.
    :var path: Path to be repeated at each intermediate depth.
    :var z_step: Function to perform cut to step's depth.
    """
    def __init__(self, tool_ctx, depth, x=0, y=0, z_step=gc.z):
        """
        Create depth repeater context manager.

        :param tool_ctx: Current tool context.
        :param depth: Depth to be processed.
        :param x: X position before cutting starts.
        :param y: Y position before cutting starts.
        """
        self.tool_ctx = tool_ctx
        self.depth = depth
        self.x = x
        self.y = y
        self.path = []
        self._z_step = z_step


    def __enter__(self):
        return self


    def __exit__(self, *args):
        ctx = self.tool_ctx
        plunge_rate = ctx.plunge_rate
        tool = ctx.tool
        steps = distance_step(self.depth, ctx.cut_depth)

        d = steps[0]
        xy = [
            gc.rapid_x(self.x) if self.x else None,
            gc.rapid_y(self.y) if self.y else None,
        ]
        tp = [gc.rapid_z(-tool.distance)]
        z = self._z_step
        path = xy + tp + [z(-d, plunge_rate)] + self.path
        path.extend(xy + [z(-d, plunge_rate)] + self.path for d in steps[1:])
        self.path = path

class DCadError(Exception):
    pass


# vim: sw=4:et:ai
