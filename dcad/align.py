# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Shape alignment.
"""

import math
from copy import copy

import dcad.gcode as gc


def within(inner, outer, sep):
    """
    Shape `inner` within dimensions of shape `outer`.

    :param inner: Internal shape.
    :param outer: External shape.
    :param sep: Length between internal edges of `outer` and external edges
        of `outer`.
    """
    if isinstance(outer, tuple):
        width, height = outer
        border = 0
        cr = 0
        pad_x, pad_y = 0, 0
    else:
        border = outer.border
        width = outer.width
        height = outer.height
        pad_x, pad_y = outer.pad
        if hasattr(outer, 'radius'):
            cr = outer.radius
        else:
            cr = 0

    shape = copy(inner)

    # `dc` gives space between shapes
    if cr:
        # corner radius compensation; note that corner radius of outer shape
        # is the same outside and inside the shape
        # align centre of each radius and then:
        # width = outer.width - 2 * border - 2 * cr
        # width += 2 * ir + 2 * (cr - ir - sep) / sqrt(2)
        ir = inner.radius
        crc = (cr - ir - sep) / math.sqrt(2)
        dc = cr + border - ir - crc # change sign to allow alignment and
                                    # substraction below
    else:
        dc = sep + border

    shape.width = width - dc * 2
    shape.height = height - dc * 2
    shape.pad = (dc + pad_x, dc + pad_y)

    return shape


__all__ = ['within']

# vim: sw=4:et:ai
