# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from dcad.render import gcode, distance_step, distance_dec, DepthRepeater
import dcad.gcode as gc
from dcad.shape import *
from dcad.tool import ToolContext, Tool


def _concat(*args):
    return '\n'.join(args)


def test_distance_step():
    v = distance_step(1.1, 0.5)
    assert [0.5, 0.5, 0.1] == v


def test_distance_step_zero_rest():
    v = distance_step(1.0, 0.5)
    assert [0.5, 0.5] == v


def test_distance_dec():
    v = distance_dec(5, [0.5, 0.5, 0.1])
    assert [4.5, 4.0, 3.9] == v


def test_render_x():
    v = gcode(gc.x(1.1000000001, 10))
    assert 'G01 X1.1 F10' == v


def test_render_y():
    v = gcode(gc.y(-1.2000000001, 12))
    assert 'G01 Y-1.2 F12' == v


def test_render_z():
    v = gcode(gc.z(-1.3000000001, 13))
    assert 'G01 Z-1.3 F13' == v


def test_render_rc():
    v = gcode(gc.rc(5, 10, -5, 0, 10))
    assert 'G02 X5Y10 I-5J0 F10' == v


def test_render_rcc():
    v = gcode(gc.rc(5, 10, 0, -5, 12))
    assert 'G02 X5Y10 I0J-5 F12' == v


def test_render_list():
    # `None` should be ignored
    v = gcode([gc.x(5, 2), gc.y(-1, 4), None])
    assert 'G01 X5 F2\nG01 Y-1 F4' == v


def test_render_tuple():
    v = gcode((gc.x(-2, 3), gc.y(3, 5)))
    assert 'G01 X-2 F3\nG01 Y3 F5' == v


def test_render_rect():
    with ToolContext(Tool(2, 1.5), 40) as ctx:
        ctx.cut_depth = 1
        r = Rect(30, 20, depth=2, border=2)
        v = gcode(r)

        rect = """\
G01 Z-1 F20
G01 X28 F40
G01 Y-18 F40
G01 X-28 F40
G01 Y18 F40\
"""
        expected = _concat(
            'G00 X-14',
            'G00 Y9',
            'G00 Z-1.5',
            rect,
            rect,
            'G00 Z3.5',
            'G00 X14',
            'G00 Y-9',
        )
        assert expected == v


def test_render_rect_border_1():
    with ToolContext(Tool(2, 1.5), 60) as ctx:
        ctx.cut_depth = 1
        r = Rect(30, 20, 2, border=2.3)
        v = gcode(r)

        rect = _concat(
            'G01 Z-1 F30',
            'G01 X27.4 F60',
            'G01 Y-17.4 F60',
            'G01 X-27.4 F60',
            'G01 Y17.4 F60',
        )
        expected = _concat(
            'G00 X-13.7',
            'G00 Y8.7',
            'G00 Z-1.5',
            rect,
            rect,
            'G01 X-0.3 F60', # last pass
            'G01 Y0.3 F60',
            'G01 X28 F60',
            'G01 Y-18 F60',
            'G01 X-28 F60',
            'G01 Y18 F60',
            'G00 Z3.5',
            'G00 X14',
            'G00 Y-9',
        )
        assert expected == v


def test_render_rect_border_2():
    with ToolContext(Tool(2, 1.5), 50) as ctx:
        ctx.cut_depth = 1
        r = Rect(30, 20, 2, border=2.6)
        v = gcode(r)

        rect = _concat(
            'G01 Z-1 F25',
            'G01 X26.8 F50',
            'G01 Y-16.8 F50',
            'G01 X-26.8 F50',
            'G01 Y16.8 F50',
            'G01 Z-1 F25',
            'G01 X26.8 F50',
            'G01 Y-16.8 F50',
            'G01 X-26.8 F50',
            'G01 Y16.8 F50',
        )
        expected = _concat(
            'G00 X-13.4',
            'G00 Y8.4',
            'G00 Z-1.5',
            rect,
            'G01 X-0.5 F50',     # complete border
            'G01 Y0.5 F50',
            'G01 X27.8 F50',
            'G01 Y-17.8 F50',
            'G01 X-27.8 F50',
            'G01 Y17.8 F50',
            'G01 X-0.1 F50',
            'G01 Y0.1 F50',
            'G01 X28 F50',
            'G01 Y-18 F50',
            'G01 X-28 F50',
            'G01 Y18 F50',
            'G00 Z3.5',
            'G00 X14',
            'G00 Y-9',
        )
        assert expected == v


def test_render_rect_corner():
    with ToolContext(Tool(2, 1.5), 38) as ctx:
        ctx.cut_depth = 1
        r = Rect(32, 22, 2, border=2, radius=6)
        v = gcode(r)

        rect = _concat(
            'G01 Z-1 F19',
            'G01 X20 F38',
            'G02 X5Y-5 I0J-5 F38',
            'G01 Y-10 F38',
            'G02 X-5Y-5 I-5J0 F38',
            'G01 X-20 F38',
            'G02 X-5Y5 I0J5 F38',
            'G01 Y10 F38',
            'G02 X5Y5 I5J0 F38',
        )
        expected = _concat(
            'G00 X-10',
            'G00 Y10',
            'G00 Z-1.5',
            rect,
            rect,
            'G00 Z3.5',
            'G00 X10',
            'G00 Y-10',
        )
        assert expected == v


def test_render_rect_corner_border():
    with ToolContext(Tool(2, 1.5), 20) as ctx:
        ctx.cut_depth = 1
        r = Rect(32, 22, 2, border=2.3, radius=8)
        v = gcode(r)

        rect = _concat(
            'G01 Z-1 F10',
            'G01 X15.4 F20',
            'G02 X7Y-7 I0J-7 F20',
            'G01 Y-5.4 F20',
            'G02 X-7Y-7 I-7J0 F20',
            'G01 X-15.4 F20',
            'G02 X-7Y7 I0J7 F20',
            'G01 Y5.4 F20',
            'G02 X7Y7 I7J0 F20',
        )
        expected = _concat(
            'G00 X-7.7',
            'G00 Y9.7',
            'G00 Z-1.5',
            rect,
            rect,
            'G01 X-0.3 F20',  # complete border
            'G01 Y0.3 F20',
            'G01 X16 F20',
            'G02 X7Y-7 I0J-7 F20',
            'G01 Y-6 F20',
            'G02 X-7Y-7 I-7J0 F20',
            'G01 X-16 F20',
            'G02 X-7Y7 I0J7 F20',
            'G01 Y6 F20',
            'G02 X7Y7 I7J0 F20',
            'G00 Z3.5',
            'G00 X8',
            'G00 Y-10',
        )
        assert expected == v

def test_bore():
    b = Bore(4, 4)
    with ToolContext(Tool(4, 1.5), 45):
        v = gcode(b)
        expected = _concat(
            'G00 Z-1.5',
            'G01 Z-4 F22.5',
            'G00 Z5.5',
        )
        assert expected == v

def test_bore_within_tool_diameter():
    b = Bore(3.2, 4) # tool diamter is 4
    with ToolContext(Tool(2, 1.5), 45):
        v = gcode(b)
        expected = _concat(
            'G00 X-0.6',
            'G00 Z-1.5',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G01 Z-0.5 F22.5',
            'G02 X0Y0 I0.6J0 F45',
            'G00 Z5.5',
            'G00 X0.6',
        )
        assert expected == v

def test_frame_of_bore():
    b = Bore(2, 4)
    n, m = 4, 3
    f = FrameOf(b, n, m, 20, 10)
    with ToolContext(Tool(2, 1.5), 50):
        v = gcode(f)

        def fmt(m, d, n, end=True):
            path = [
                'G00 Z-1.5',
                'G01 Z-4 F25',
                'G00 Z5.5',
                'G00 {m}{d}'.format(m=m, d=d)
            ] * n
            if not end:
                path.pop()
            return _concat(*path)

        #  y -> x -> x -> x
        #  ^              |
        #  y              y
        #  ^              |
        #  x <- x <- x <- y
        expected = _concat(
            'G00 X-9',
            'G00 Y4',
            fmt('X', 6, n - 1),
            fmt('Y', -4, m - 1),
            fmt('X', -6, n - 1),
            fmt('Y', 4, m - 1, end=False),
            'G00 X9',
        )
        assert expected == v

def test_frame_of_bore_2x2():
    b = Bore(2, 4)
    n, m = 2, 2
    f = FrameOf(b, n, m, 20, 10)
    with ToolContext(Tool(2, 1.5), 50):
        v = gcode(f)

        def fmt(m, d, n, end=False):
            path = [
                'G00 Z-1.5',
                'G01 Z-4 F25',
                'G00 Z5.5',
                'G00 {m}{d}'.format(m=m, d=d)
            ] * n
            if end:
                path.pop()
            return _concat(*path)

        #  y -> -> x
        #  ^       |
        #  ^       |
        #  x <- <- y
        expected = _concat(
            'G00 X-9',
            'G00 Y4',
            fmt('X', 18, n - 1),
            fmt('Y', -8, m - 1),
            fmt('X', -18, n - 1),
            fmt('Y', 8, m - 1, end=True),
            'G00 Y4',
            'G00 X9',
        )
        assert expected == v

def test_depth_repeater():
    tool = Tool(5, 2)
    with ToolContext(tool, 60) as ctx:
        with DepthRepeater(ctx, 4.6) as dr:
            dr.path.extend([
                gc.x(11, 12),
                gc.y(12, 14),
            ])

        v = gcode(dr.path)
        path = 'G01 Z-1.25 F30\nG01 X11 F12\nG01 Y12 F14'
        expected = _concat(
            'G00 Z-2',
            path,
            path,
            path,
            'G01 Z-0.85 F30', # 4.6 - 3.75
            'G01 X11 F12',
            'G01 Y12 F14',
        )
        assert expected == v


def test_depth_repeater_xy():
    tool = Tool(5, 2)
    with ToolContext(tool, 80) as ctx:
        with DepthRepeater(ctx, 4.6, x=7, y=6) as dr:
            dr.path.extend([
                gc.x(11, 20),
                gc.y(12, 30),
            ])

        v = gcode(dr.path)
        path = 'G00 X7\nG00 Y6\nG01 Z-1.25 F40\nG01 X11 F20\nG01 Y12 F30'
        expected = _concat(
            'G00 X7',
            'G00 Y6',
            'G00 Z-2',
            'G01 Z-1.25 F40',
            'G01 X11 F20',
            'G01 Y12 F30',
            path,
            path,
            'G00 X7',
            'G00 Y6',
            'G01 Z-0.85 F40', # 4.6 - 3.75
            'G01 X11 F20',
            'G01 Y12 F30',
        )
        assert expected == v


POCKET_SPIRAL = """\
G01 Y7.5 F30
G01 X35 F30
G01 Y-15 F30
G01 X-42.5 F30
G01 Y22.5 F30
G01 X50 F30
G01 Y-30 F30
G01 X-50 F30\
"""

def test_pocket_depth_single():
    with ToolContext(Tool(10, 1.5), 30):
        # note: all cut paths are equally wide for 60x40 pocket, tool 10mm
        # and cut width 7.5mm (0.75)
        pocket = Pocket(60, 40, depth=2.5, radius=5)
        v = gcode(pocket)
        expected = _concat(
            'G00 X-17.5',
            'G00 Z-1.5',
            'G01 Z-1.25 F15', # slot
            'G01 X35 F30',
            'G01 Z-1.25 F15',
            'G01 X-35 F30',
            POCKET_SPIRAL,
            'G01 Y15 F30',
            'G01 X7.5 F30',
            'G00 Z4',
            'G00 X17.5',
        )
    assert expected == v

def test_pocket_small():
    with ToolContext(Tool(10, 1.5), 30):
        pocket = Pocket(16, 12, depth=2.5)
        v = gcode(pocket)
        expected = _concat(
            'G00 X-3',
            'G00 Z-1.5',
            'G01 Z-1.25 F15',
            'G01 X6 F30',
            'G01 Z-1.25 F15',
            'G01 X-6 F30',
            'G01 Y1 F30',
            'G01 X6 F30',
            'G01 Y-2 F30',
            'G01 X-6 F30',
            'G01 Y1 F30',
            'G01 X0 F30',
            'G00 Z4',
            'G00 X3',
        )
    assert expected == v

def test_pocket_depth_repeat():
    with ToolContext(Tool(10, 1.5), 30):
        pocket = Pocket(60, 40, depth=7.0, radius=5)
        v = gcode(pocket)
        expected = _concat(
            'G00 X-17.5',
            'G00 Z-1.5',
            'G01 Z-1.25 F15', # slot
            'G01 X35 F30',
            'G01 Z-1.25 F15',
            'G01 X-35 F30',
            POCKET_SPIRAL,
            'G01 Y15 F30',
            'G01 X7.5 F30',
            'G01 Z-1.25 F15', # slot
            'G01 X35 F30',
            'G01 Z-1.25 F15',
            'G01 X-35 F30',
            POCKET_SPIRAL,
            'G01 Y15 F30',
            'G01 X7.5 F30',
            'G01 Z-1 F15', # slot
            'G01 X35 F30',
            'G01 Z-1 F15',
            'G01 X-35 F30',
            POCKET_SPIRAL,
            'G01 Y15 F30',
            'G01 X7.5 F30',
            'G00 Z8.5',
            'G00 X17.5',
        )
    assert expected == v


# vim: sw=4:et:ai
