# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from dcad.align import *
from dcad.shape import *
from dcad.tool import Tool

def test_within_tuple():
    rect = Rect(20, 15, 1)

    r = within(rect, (15, 12), 1)

    assert (1, 1) == r.pad
    assert 13 == r.width
    assert 10 == r.height


def test_within_rect():
    inner = Rect(30, 25, 1)
    outer = Rect(20, 15, 1, border=2)

    r = within(inner, outer, 1)

    assert (3, 3) == r.pad
    assert 14 == r.width
    assert 9 == r.height


def test_within_frame():
    rect = Rect(30, 25, 1)
    hole = Bore(3, 1)
    frame = FrameOf(hole, 3, 2, 20, 18)

    r = within(rect, frame, 2)

    assert (5, 5) == r.pad
    assert 10 == r.width
    assert 8 == r.height


def test_within_rect_pocket_corner():
    """
    Test when radius difference is equal to 1 and sep=1.
    """
    gland = Rect(80, 52, 1, border=2.3, radius=6)
    pocket = Pocket(0, 0, 1, radius=5)

    p = within(pocket, gland, 1)
    x, y = p.pad

    assert abs(3.3 - x) < 0.001
    assert abs(3.3 - y) < 0.001
    assert 73.4 == round(p.width, 5)
    assert 45.4 == round(p.height, 5)


def test_within_rect_pocket_corner_2():
    """
    Test when radius difference is equal to 2 and sep=1.
    """
    gland = Rect(80, 52, 1, border=2.3, radius=7)
    pocket = Pocket(0, 0, 1, radius=5)

    p = within(pocket, gland, 1)
    x, y = p.pad

    assert abs(3.59289 - x) < 0.001
    assert abs(3.59289 - y) < 0.001
    # 2 * (40 - 2.3 - 7 + 5 + (7 - 5 - 1) / math.sqrt(2))
    assert 72.81421 == round(p.width, 5)
    assert 44.81421 == round(p.height, 5)


def test_within_rect_pocket_corner_3():
    """
    Test when radius difference is equal to 3 and sep=1.
    """
    gland = Rect(80, 52, 1, border=2.3, radius=7)
    pocket = Pocket(0, 0, 1, radius=4)

    p = within(pocket, gland, 1)
    x, y = p.pad

    assert abs(3.88579 - x) < 0.00001
    assert abs(3.88579 - y) < 0.00001
    # 2 * (40 - 2.3 - 7 + 4 + (7 - 4 - 1) / math.sqrt(2))
    assert 72.22843 == round(p.width, 5)
    assert 44.22843 == round(p.height, 5)


def test_within_rect_pocket_corner_4():
    """
    Test when radius difference is equal to 3 and sep=2.
    """
    gland = Rect(80, 52, 1, border=2.3, radius=7)
    pocket = Pocket(0, 0, 1, radius=4)

    p = within(pocket, gland, 2)
    x, y = p.pad

    assert abs(4.59289 - x) < 0.00001
    assert abs(4.59289 - y) < 0.00001
    # 2 * (40 - 2.3 - 7 + 4 + (7 - 4 - 2) / math.sqrt(2))
    assert 70.81421 == round(p.width, 5)
    assert 42.81421 == round(p.height, 5)


# vim: sw=4:et:ai
