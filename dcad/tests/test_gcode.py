# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import dcad.gcode as gc

import pytest

def test_linear_move():
    assert 'G01 X1.2 F35' == gc.x(1.2000000001, 35.0000000001)
    assert 'G01 Y1.2 F25' == gc.y(1.2000000001, 25)
    assert 'G01 Z1.2 F35' == gc.z(1.2000000001, 35)


def test_rapid_pos():
    assert 'G00 X1.2' == gc.rapid_x(1.2000000001)
    assert 'G00 Y1.2' == gc.rapid_y(1.2000000001)
    assert 'G00 Z1.2' == gc.rapid_z(1.2000000001)


# vim: sw=4:et:ai
