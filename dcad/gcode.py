# dcmod - modular dive computer
#
# Copyright (C) 2013-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
G-code primitives.
"""

def register(name, fmt, speed=False, registry=None):
    """
    Register G-code primitive.

    Creates function in global namespace (or in registry), which formats
    G-code primitive for given parameters.

    :param name: Function name of G-code primitive.
    :param fmt: G-code primitive formatter.
    """
    if registry is None:
        registry = globals()

    registry[name] = fmt.format


register('comment', '({})')
register('pause', 'M0 ({})')
register('home', 'G28')
register('relative', 'G91')
register('speed', 'F{:.6g}')
register('x', 'G01 X{:.6g} F{:.6g}')
register('y', 'G01 Y{:.6g} F{:.6g}')
register('z', 'G01 Z{:.6g} F{:.6g}')
register('rapid_x', 'G00 X{:.6g}')
register('rapid_y', 'G00 Y{:.6g}')
register('rapid_z', 'G00 Z{:.6g}')
register('rc', 'G02 X{:.6g}Y{:.6g} I{:.6g}J{:.6g} F{:.6g}')
register('rcc', 'G03 X{:.6g}Y{:.6g} I{:.6g}J{:.6g} F{:.6g}')

# vim: sw=4:et:ai
